<?php


require_once('../../AddTextToPDF-0.9.0/src/AddTextToPDF.php');

// Data to be printed in the PDF		
$vars =  array (
				'olr' => 'OLR-K-PNR-HTR4-3E5', 
				'serialNo' => '36239900', 
				'deliveryDate' => '2021/04/19',

				'Cname' => 'Eliel Saarinen',
				'Cstreet' => 'Kaivokatu',
				'Cnum' => '1',
				'Ccity' => '0100 Helsinki',
				'Cdate' => '2021/07/28',
				'Ccountry' => 'Finland',

			); 		


$newPDF = new AddTextToPDF("warrantyPDF/Warranty-Test-1.json", $vars);


$newPDF->execute();


?>
