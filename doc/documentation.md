
# AddTextToPDF 

Open an existing PDF file and add text to it.

- [AddTextToPDF](#addtexttopdf)
  - [AddTextToPDF Class](#addtexttopdf-class)
    - [Constructor](#constructor)
    - [execute()](#execute)
  - [Usage](#usage)
  - [JSON cfg file](#json-cfg-file)
    - [Top-level "pdf" JSON list](#top-level-pdf-json-list)
    - [font](#font)
    - [page](#page)



## AddTextToPDF Class


---------------------------------------------

### Constructor

---------------------------------------------

```php
    AddTextToPDF(string $name_of_the_JSON_file, associative_array $vars);
```
- Parameters:   
  - **$name_of_the_JSON_file**:  JSON file pathname
  - **$vars**: Associative array containing data to be *printed* in the Output PDF

- Errors
  - if
    - the JSON config file ($name_of_the_JSON_file) cannot be opened
    - the source PDF file (declared in the JSON config file) cannot be opened
  - **trigger_error($msg, E_USER_ERROR);**

---------------------------------------------

### execute()

---------------------------------------------

```php
    execute($dest='', $name='', $isUTF8=false) 
```
 
- Parameters: Passed **as is** to fpdf::Output() -- [see lib docs](http://www.fpdf.org/en/doc/output.htm)
    - dest
      - Destination where to send the document. It can be one of the following:
        - I: send the file inline to the browser. The PDF viewer is used if available.
        - D: send to the browser and force a file download with the name given by name.
        - F: save to a local file with the name given by name (may include a path).
        - S: return the document as a string.
      - **The default value is I** 
  - name
      - The name of the file. It is ignored in case of destination S.
    - **The default value is doc.pdf** 
  - isUTF8
      - Indicates if name is encoded in ISO-8859-1 (false) or UTF-8 (true). Only used for destinations I and D.
    - **The default value is false**


---------------------------------------------

## Usage

---------------------------------------------


```php
<?php

// Assume AddTextToPDF source tree is at the same level of this ".php" file
require_once('AddTextToPDF-0.9.0/src/AddTextToPDF.php');

// [$vars]: an "associative array" containigs the data to be printed in the PDF 
//          It usually comes from a DB 
//          Statically initialized below with sample data to test the lib
$vars =  array (
			'serialNo' => '36239900', 
			'Cname' => 'Eliel Saarinen',
			'Cstreet' => 'Kaivokatu',
			'Cnum' => '1',
			'Ccity' => '0100 Helsinki',
			'Cdate' => '2021/07/28',
			'Ccountry' => 'Finland',
		);
// Create obj with instructions from JSON cfg file
//    [Warranty-1.json]: JSON file pathname
$newPDF = new AddTextToPDF("Warranty-1.json", $vars);

// calls execute to process instructions contained in the JSON file and Generate the new PDF
// Called with no parameters, execute() will send the optput PDF to the User's browser
$newPDF->execute();

?>

```



## JSON cfg file 

The JSON config file is used read by the **execute()** method. This method loops through "**page**" array and:
  - Load "**srcpagenum**" from the source PDF and *add* it to the output PDF (current page)
  - Loop through "**addtext**" items and execute *print* instructions on the current page


The JSON file below, taken from example-2, contains an example for every different configurable value.

```json
{
 "pdf": [
   {		 
     "srcPDF" : "./warrantyPDF/Warranty-Test.pdf",
     "font"	: {"name":"helvetica", "attr":"B", "size":10} ,
     "color"	: [0,0,0],
     "page": [
        {  "srcpagenum" : "1",
           "addtext" : [
              {"x":14, "y":23, "var":"Cname" ,
                 "font"	: [
                   {"uptolen":70, "name":"helvetica", "attr":"B", "size":8},
                   {"uptolen":40, "name":"helvetica", "attr":"B", "size":10}
                  ] 
               },
               {"x":14, "y":34, "var":"Cstreet" },
               {"x":92, "y":34, "var":"Cnum" },
               {"x":14, "y":45, "var":"Ccity", "color"	: [254,0,0] },
               {"x":83, "y":45, "var":"Ccountry" },
               {"x":14, "y":56, "var":"Cdate" },
   
               {"x":14, "y":104, "var":"olr", "font" : {"name":"helvetica", "attr":"BU", "size":14} },
               {"x":106, "y":105, "var":"serialNo" },
               {"x":14, "y":119, "var":"deliveryDate" , 
                  "color"	: [0,50,254], 
                  "font": [
                    {"uptolen":70, "name":"helvetica", "attr":"BI", "size":8},
                   {"uptolen":40, "name":"helvetica", "attr":"BI", "size":10}
                  ] 
               }
            ]
        },
		    {   "srcpagenum" : "2" ,
           "addtext" : [
               {"x":14, "y":263, "text":"Fixed text added to page 2 in JSON cfg file SET Font and Color" ,  
                "color": [164,64,64],
                "font": [
                   {"uptolen":70, "name":"helvetica", "attr":"B", "size":12},
                   {"uptolen":40, "name":"helvetica", "attr":"B", "size":14}
                ] 
             }
           ]
        },
        { "srcpagenum" : "3", "addtext_at_pageidx":0 },
        { "srcpagenum" : "4" }
	   ]
   }
 ]
}
```

---------------------------------------------

### Top-level "pdf" JSON list

---------------------------------------------

Each JSON Object contained in the "pdf" list represents one source PDF file.

If, as usual, you have just one multi-page PDF 'source', the list will contain only one item.
> *In case your "source" PDF document is splitted in more than one PDF (ex: one PDF for each page), you can **merge** all the source PDFs in a single PDF output file.*

In every JSON object of the list you need to specify at least 2 **mandatory** "keys":
* "**srcPDF**": Pathname of the PDF document used as a template to create the new PDF (with added text)
* "**page**": array() of "**page objects**" *-- see below for complete syntax*.  

and you can use 2 optional "keys": 

* "**font**": Defines the default **Font(type,size,attributes)** used to render any text in the page *(when a different Font is not specified for the single text/variable)* *-- see below for complete syntax*.
* "**color**": array() with the RGB values for the default color used to render any text in the page *(when a different Color is not specified for the single text/variable)* 


---------------------------------------------

### font 

---------------------------------------------

1) Single font (as seen in the example at the top level, for the default font)
 
   ```json  "font"	: {"name":"helvetica", "attr":"B", "size":10} ,```
3) Font list (array) - The font to use will be chosen according to length of the Text to print 
  (useful to have long texts fits into a fixed area)
    ```json
        "font": [
			{"uptolen":70, "name":"helvetica", "attr":"B", "size":8},
			{"uptolen":40, "name":"helvetica", "attr":"B", "size":10},
			{"uptolen":20, "name":"helvetica", "attr":"B", "size":14}
         ] 
    ```
   - **uptolen**: 'n' - Text shorter than 'n' characters will be printed with this font
     - In the example above:
      - 18 chars string will be printed with font size 14
      - 35 chars string will be printed with font size 10
      - 55 chars string will be printed with font size 8
      - 85 chars string will be printed with the **FIRST font of the list** 
        - ***A String longer than the bigger "uptolen" will be printed with the FIRST font of the list (so usually you want to have the 'smallest' font at the first position)***



- **name**: Font name
  - Courier (fixed-width)
  - Helvetica or Arial (synonymous; sans serif)
  - Times (serif)
  - Symbol (symbolic)
  - ZapfDingbats (symbolic)

- **attr**: Attributes "B":Bold, "I":Italic, "U":Underline
  - "" for no attributes (regular)
  - "BU" = Bold and Underline
  - etc...
- **size**: Font Size



> **Please Note: (name,attr,size)** are passes to the SetFont method of the FPFD library 
>
>   See [SetFont() documentation] (http://www.fpdf.org/en/doc/setfont.htm) for reference.
>
>   To add more fonts, etc [see FPDF library documentation](http://www.fpdf.org/)


---------------------------------------------

### page 

---------------------------------------------

- **srcpagenum**: Page num to read from the Source PDF - added at the current page position of the output file
  - "srcpagenum": 1 - Load page 1 from the source PDF

>> This is the **only mandatory field of the page** object.
>> If you have a 4-pages source PDF and you want to produce a new PDF with just 2 pages (page 1 and 3 of the souce PDF)
>> the **page** section of your JSON will be:
```json
    "page": [
			{ 	"srcpagenum" : "1" },
			{ 	"srcpagenum" : "3" }
	]
```

- **addtext**: array of JSON objects defining the Texts to be added 
  ```json
    "page": [
		{ "srcpagenum" : "1" },
        {   
            "srcpagenum" : "2" ,
            "addtext" : [
		       {"x":106, "y":105, "var":"CustomerName" },
		       {"x":14, "y":263, "text":"Fixed text added to current page" }
              ]
        }
    ]
  ```
    - **x**: x position of the text
    - **y**: y position of the text
    - **[var]**: The *type* of text to print is a **Variable** (an element of the associative array)
      - ex: **"var":"CustomerName"** - the associative array passed to execute() method has an element named "CustomerName" (CustomerName=>"Harry Tuttle")
    - **[text]**: The *type* of text to print is a **Fixed Text** contained in the "text":value


> If a specific **addtext** element need to be printed with a different Font/Color (not the default one defined at top level) **you can include "font", "color" into "addtext" object** --see example at the beginning of the [JSON Config file](#json-cfg-file) section




- **addtext_at_pageidx**: the current output page *copy* "addtext" definitions from a prevoius page
  ```json
    "page": [
        {   
            "srcpagenum" : "1" ,
            "addtext" : [
		       {"x":106, "y":105, "var":"CustomerName" },
		       {"x":106, "y":135, "var":"CustomerAddress" },
              ]
        },
		{ "srcpagenum" : "2" },
		{ "srcpagenum" : "3", "addtext_at_pageidx":0 },
		{ "srcpagenum" : "4" }
    ]
  ```
> In the example above, "souce PDF pages 1 and 3" contains the same 'form area' to be filled with the same user data.
> 
> So the third page of the output PDF 'copies' the "addtext" defined in Output Page 1 (idx=0)



