<?php

/**
 * Add Text to PDF class
 *
 * @license   http://opensource.org/licenses/mit-license The MIT License
 */

use setasign\Fpdi\Fpdi;

require_once('fpdf183/fpdf.php');
require_once('FPDI-2.3.6/src/autoload.php');


class AddTextToPDF {
	
	const VERSION = '0.9.0';

	var $cfgfile='';
	var $variables = array();
	var $json_o=null;
	/* 
	 * Constructor
	 */
	function AddTextToPDF ($cfgfile, $data=null){
		$this->cfgfile = $cfgfile;
		$this->setData($data);

		// Load json cfg file
		$cfgstr = file_get_contents($this->cfgfile);
		if($cfgstr === false){
			trigger_error("Open error for Config file:[" . $this->cfgfile . "]" , E_USER_ERROR);
		}
		$this->json_o = json_decode($cfgstr);

		$tnum = count($this->json_o->pdf);
		for ($i = 0; $i < $tnum; $i++){
			if ( ! is_readable($this->json_o->pdf[$i]->srcPDF)) {
				trigger_error("Open error for PDF source file:[" . $this->json_o->pdf[$i]->srcPDF . "]" , E_USER_ERROR);
			}
		}
	}

	/* Set the Associative array with data to be used in execute()
	 */
	function setData ($data){
		$this->variables = $data;
	}


	/* - Load a PDF file 
	 *	- Loop through page numbers listed in JSON cfg file and:
	 *		- add page 'srcpagenum' to the output document
	 *		- add optional text/variables to the page 'n'
	 *	- Write modified PDF 
	 *
	 *		Parameters: (parameters passed as is to fpdf::Output() -- see: http://www.fpdf.org/en/doc/output.htm)
	 *			dest
     * 			Destination where to send the document. It can be one of the following:
     *   				I: send the file inline to the browser. The PDF viewer is used if available.
     *   				D: send to the browser and force a file download with the name given by name.
     *   				F: save to a local file with the name given by name (may include a path).
     *   				S: return the document as a string.
     * 		The default value is I. 
     *
 	 *			name
     * 			The name of the file. It is ignored in case of destination S.
     * 		The default value is doc.pdf. 
	 *
	 *			isUTF8
     * 			Indicates if name is encoded in ISO-8859-1 (false) or UTF-8 (true). Only used for destinations I and D.
     * 		The default value is false. 
	 *		
	 */
	function execute($dest='', $name='', $isUTF8=false) {

		if ( $this->variables == null) {
			trigger_error("Associtive Array with data not set." , E_USER_ERROR);
		}
		$pdf = new Fpdi();
	
		// loop through PDF-source files list 
		$tnum = count($this->json_o->pdf);
		for ($t = 0; $t < $tnum; $t++) {
			$currentPDF = $this->json_o->pdf[$t];
	
			$pdf->setSourceFile($currentPDF->srcPDF); 
	
			// Default color
			$pdf->SetTextColor($currentPDF->color[0], $currentPDF->color[1], $currentPDF->color[2], );
	
			// Loop through page numbers listed in JSON cfg file 
			$pnum = count($currentPDF->page);
			for ($i = 0; $i < $pnum; $i++){
		
				// Load page 'srcpagenum' from source PDF and add it to output PDF	
				$pdf->AddPage();
				$tpl = $pdf->importPage($currentPDF->page[$i]->srcpagenum);
				$pdf->useTemplate($tpl, ['adjustPageSize' => true]);
			
				// Load optional 'addtext' with text/var to add to output
				$addtext = (isset($currentPDF->page[$i]->addtext)) ? $currentPDF->page[$i]->addtext : array();
				$lnum = count($addtext);
				// Check if the page have a "addtext_at_pageidx" field (Copy addtext declaration from another JSON->page[])
				if($lnum == 0 && isset($currentPDF->page[$i]->addtext_at_pageidx)){
					$lineIdx=$currentPDF->page[$i]->addtext_at_pageidx;
					$addtext = (isset($currentPDF->page[$lineIdx]->addtext)) ? $currentPDF->page[$lineIdx]->addtext : array();
					$lnum = count($addtext);
				}
	
				// Process optionale text/vars 'addtext' for the current page
				for ($j = 0; $j < $lnum; $j++){
					$text='';
					if(isset($addtext[$j]->text)) {
						$text = $addtext[$j]->text;
					} else if(isset($addtext[$j]->var)) {
						$text = $this->variables[$addtext[$j]->var];
					}
					// Set font (line-specific font if defined or globally defined one)
					if(isset($addtext[$j]->font)) {
						$this->isetFont($pdf, $addtext[$j]->font, strlen($text));
					} else {
						$this->isetFont($pdf, $currentPDF->font, strlen($text) );
					}
					// Set line-specifid color if defined
					if(isset($addtext[$j]->color)) {
						$pdf->SetTextColor($addtext[$j]->color[0], $addtext[$j]->color[1], $addtext[$j]->color[2]);
					}
					// Write output text
					$pdf->SetXY($addtext[$j]->x, $addtext[$j]->y);
					$pdf->Write(0, $text);
	
					// reset to default color
					if(isset($addtext[$j]->color)) {
						$pdf->SetTextColor($currentPDF->color[0], $currentPDF->color[1], $currentPDF->color[2], );
					}
				}
			}
		}	
		// PDF output
		switch(strtoupper($dest)) {
			case 'S':
				return($pdf->Output($dest, $name, $isUTF8));
			default:
				$pdf->Output($dest, $name, $isUTF8);
				return('');
		}

	}


	/* If the string contained in a specific field of the Associative Array is longer than [$maxl], split it
	 *
	 *	Parameters: 
	 *		&$aarr: Reference to the Associative Array containing the data
     *			The associative array is modified by this method 
	 *
 	 *		maxl: 	Max length
     * 			Max length allowed for the string contained in the specific 'field' of the AssociativeArray 
	 *
	 *		$keys: String array
     * 			An array of strings representing the keys of the associative array "&$aarr" involved in the process
	 * 			$keys[0] : Field containing the String to be analized (the one that may need to be splitted)
	 * 			$keys[1,...,n] : "keys" of the associative array used to store the 'splitted' substrings  
	 *	Returns:
	 *		 0: No need to split (string still in field $aarr[$keys[0]]) 	
	 *		 1: Splitted in fields (strins in fields $aarr[$keys[1-n]]) 	
	 *		-1: Splitted but last field contains MORE THAN $maxl chars (not enough fields/keys supplied in $keys)
	*/
	function formatField(&$aarr, $maxl, $keys ) {
		// total number of output 'fields'  
		$out_fld_num = count($keys) - 1 ; 
		// create empty fields for the keys used to split (avoid warnings in execute() in case no split needed)
		for ($i = 1; $i <= $out_fld_num ; $i++){		
			$aarr[$keys[$i]] = ' '; 
		}
		// The input field need to be splitted ?
		if(strlen($aarr[$keys[0]]) > $maxl ){
			$tmp_a = explode("\n", wordwrap($aarr[$keys[0]], $maxl));
			$aarr[$keys[0]] = ''; 						// Remove input field content
			for ($i = 1; $i < $out_fld_num ; $i++){		// Assign 'parts' to the available fields list (but the last one field)
				if($i <= count($tmp_a)) {
					$aarr[$keys[$i]] = $tmp_a[$i-1]; 
				} else {
					$aarr[$keys[$i]] = ''; 
				}
			}
			if($i <= count($tmp_a)){
				// Assign last field with the the rest of $tmp_a items
				$aarr[$keys[$out_fld_num]] = join("", array_slice($tmp_a,$out_fld_num - 1 )); 
			}
			if(count($keys) > count($tmp_a)){
				return(1); 
			}
			return(-1);
		}
		return(0);
	}


	/* Set font to use for a string with length $len
	In the JSON file a "single font" or "font array" syntax con be used:
	Single font:
		"font"	: {"name":"helvetica", "attr":"", "size":10},
	Fonts array	/ the font used depends on the length of the string to print
		"font"	: [
				{"uptolen":30, "name":"helvetica", "attr":"", "size":10},
				{"uptolen":50, "name":"helvetica", "attr":"", "size":8}
			],
		PLESE NOTE: 
			the first font in the list is the default one used when the length 	
			of the string does not 'fit' in any "font[i].uptolen" 
	*/
	private function isetFont($pdf, $json_font, $len) {
		if (is_array($json_font)) {
			$use_font = $json_font[0]; // Default font = firs one in the list
			$n = count($json_font);
			for ($i = 0; $i < $n; $i++) {
				// search line (font) with the minumum "uptolen" where our strlen ($len) fits 
				if (($len < $json_font[$i]->uptolen) && ($json_font[$i]->uptolen < $use_font->uptolen)) {
					$use_font = $json_font[$i];
				}
			}
			$pdf->SetFont($use_font->name, $use_font->attr, $use_font->size);
		} else {
			$pdf->SetFont($json_font->name, $json_font->attr, $json_font->size);
		}
	}


	
}
?>
